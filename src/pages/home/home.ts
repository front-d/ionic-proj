import { Component } from '@angular/core';
import { ModalController, AlertController} from 'ionic-angular';
import { AddItem } from '../modal-pages/add-item/add-item';
import {Http} from '@angular/http'
import { LoadingController, Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
  countLoadItem = 0;
  stratCountNewItem = 13;
  countNewItem = 13;
  itemsAll = [];
  items = [];
  changeTable = false;
  disablList = false;
  noMoreItemLoad = false;
  showSroll = true;


  constructor(
    public modalCtrl: ModalController,
    private http: Http,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public platform: Platform) {
    let loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Data loading...'
    });
    // start loading preload page
    loading.present();

    // load json
    this.http.get('https://api.myjson.com/bins/xjuu9')
      .map((res) => res.json())
      .subscribe(data => {
        this.itemsAll = data.elements;
        // add 13 elements in show list
        for (; this.countLoadItem < this.countNewItem; this.countLoadItem++) {
          this.items.push( this.itemsAll[this.countLoadItem]);
        }
        this.countNewItem = this.countNewItem + this.stratCountNewItem;
        // finish loading preload page
        loading.dismiss();
      }, (rej) => {console.error("Could not load local data",rej)});

  }

  //Change value pap

  presentPrompt(valueInp, ind) {
    let parse = this.alertCtrl.create({
      title: 'Change value',
      inputs: [
        {
          name: 'valueInp',
          value: valueInp
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Save',
          handler: data => {
            if (data.valueInp) {
              this.items[ind].name = data.valueInp;
            }
          }
        }
      ]
    });
    parse.present();
  }

  //change title action
  changeTitle(text,ind) {
    if (this.changeTable) {
      this.presentPrompt(text, ind);
    }
  }
  // add new item in to list
  doInfinite(infiniteScroll) {
    setTimeout(() => {
      console.log(this.countNewItem);
      for (; this.countLoadItem < this.countNewItem; this.countLoadItem++) {
        // if no item stop load
        if (this.itemsAll[this.countLoadItem]) {
          this.items.push( this.itemsAll[this.countLoadItem]);
        } else {
          infiniteScroll.complete();
          this.showSroll = false;
          return;
        }
      }
      this.countNewItem = this.countNewItem + this.stratCountNewItem;
      infiniteScroll.complete();
    }, 200);
  }

  // remove item on click
  removeItem(item){
    let index = this.items.indexOf(item);
    if(index > -1){
      this.items.splice(index, 1);
    }
  }

  // check left or right swipe
  onGesture(e, item) {
    if (e.direction == 2 && !this.disablList) {
      this.items[item].nonActive = false;
    } else {
      this.items[item].nonActive = true;
    }
  }

  // modal
  presentModal() {
    let modal = this.modalCtrl.create(AddItem);
    modal.onDidDismiss(data => {
      if (data) {
        let item = {
          name: data,
          nonActive: false
        };
        this.items.push(item);
      }
    });
    modal.present();
  }
}
