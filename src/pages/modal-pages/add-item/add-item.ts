import { Component } from '@angular/core';
import { ViewController} from 'ionic-angular';
import { HomePage } from '../../home/home';

@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html'
})


export class AddItem {
  rootPage: any = HomePage;
  myText = "";

  constructor(
    public viewCtrl: ViewController) {
  }
  close() {
    this.viewCtrl.dismiss();
  }
  edit(data) {
    this.viewCtrl.dismiss(data);
  }
}
